#-*-coding:utf-8-*-

from django.contrib import admin

from plando63.apps.pages.models import Page


class AdminPage(admin.ModelAdmin):
    list_display = ('slug','title', 'keywords')
    search_fields = ('slug','keywords', 'title')
    list_filter = ('slug','keywords', 'title')
    ordering = ('-datetime', )

admin.site.register(Page, AdminPage)