#-*-coding:utf-8-*-
from django.db import models

class Page(models.Model):
    title = models.CharField(verbose_name=u"Имя страницы", max_length=1024)
    description = models.CharField(verbose_name=u'Описание страницы', max_length=1024)
    keywords = models.CharField(verbose_name=u"Ключевые слова", max_length=1024)
    datetime = models.DateField(verbose_name=u'Дата создания')
    content = models.TextField(verbose_name=u"Содержание страницы", max_length=10000)
    slug = models.CharField(verbose_name=u"Адресная ссылка", max_length=1024)
    template = models.CharField(verbose_name=u"Шаблон", max_length=1024)
    
    def __unicode__(self):
        return '%s %s' % (self.datetime, self.title)

    def get_absolute_url(self):

        return '%s' % (self.slug)
