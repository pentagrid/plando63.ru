#-*-coding:utf-8-*-

from django.views.generic import ListView, DetailView

from plando63.apps.pages.models import Page


class PageListView(ListView):
    model= Page

class PageDetailView(DetailView):
    model =Page
    
