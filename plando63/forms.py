# -*- coding: utf-8 -*-
from django import forms


class FormContacts(forms.Form):

    name = forms.CharField(label='', widget=forms.TextInput(attrs={'placeholder': 'Ваше имя'}), error_messages={'required': '* Это поле обязательно для заполнения'})
    phone = forms.CharField(label='', widget=forms.TextInput(attrs={'placeholder': 'Укажите Ваш телефон'}), error_messages={'required': '* Это поле обязательно для заполнения'})
    email = forms.EmailField(label='', widget=forms.TextInput(attrs={'placeholder': 'Укажите Ваш e-mail'}),  error_messages={'required': '* Это поле обязательно для заполнения'})
    message = forms.CharField(label='', required=False,
                              widget=forms.Textarea(attrs={'placeholder': 'Дополнительная информация от Вас'}))