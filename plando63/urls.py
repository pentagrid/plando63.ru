from django.conf.urls import patterns, include, url
from django.views.generic.base import TemplateView, RedirectView
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
admin.autodiscover()

from plando63.apps.pages.views import PageDetailView


class TextPlainView(TemplateView):
    def render_to_response(self, context, **kwargs):
        return super(TextPlainView, self).render_to_response(
            context, content_type='text/plain', **kwargs)


urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^$', 'plando63.views.home'),
                       url(r'^contacts/$', 'plando63.views.contacts'),
                       url(r'^(?P<slug>\S+)/$', PageDetailView.as_view()),
                       url(r'^robots\.txt$', TextPlainView.as_view(template_name='robots.txt')),
                       url(r'^favicon\.ico$', RedirectView.as_view(url='/static/images/favicon.ico')),


)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)