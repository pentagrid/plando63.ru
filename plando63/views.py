# -*-coding: utf-8 -*-

from django.shortcuts import render
from django.core.mail import send_mail, BadHeaderError
from forms import FormContacts

def home(request):
    return render(request, 'home.html')

def contacts(request):
    form_contact = FormContacts(request.POST)
    if request.POST:
        if form_contact.is_valid():
            name = request.POST.get('name', '')
            phone = request.POST.get('phone', '')
            email = request.POST.get('email', '')
            message = request.POST.get('message', '')
            messages = u'Имя: ' + name + '\n' + u'Телефон клиента: ' + phone + '\n' + u'E-mail клиента: ' + email + '\n' + u'Cообщение: ' + message
            if email and name and phone:
                try:
                    send_mail('сообщение с сайта plando63.ru', messages, 'Plando63@bk.ru', ['Plando63@bk.ru'])
                except BadHeaderError:
                    return render(request, 'contacts.html',
                                  {'msg': 'Неправильный заголовок.', 'form': form_contact, 'res': 'error'})
                return render(request, 'contacts.html',
                              {'msg': 'Заявка доставлена. Наш специалист свяжется с Вами.', 'form': form_contact,
                               'res': 'seccuss'})
        return render(request, 'contacts.html',
                      {'msg': 'Сообщение не доставлено. Правильно заполните все обязательные поля',
                       'form': form_contact, 'res': 'error'})
    else:
        return render(request, 'contacts.html', {'form': form_contact, 'res': 'alarm', 'msg': 'Заполните все поля формы и отправьте сообщение'})
